package br.com.mobicare.desafio.service;

public interface PessoaService {
    void validarCpf(String cpf);
}
