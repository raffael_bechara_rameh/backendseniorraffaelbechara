package br.com.mobicare.desafio.service.impl;

import br.com.mobicare.desafio.domain.Setor;
import br.com.mobicare.desafio.repository.SetorRepository;
import br.com.mobicare.desafio.service.SetorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetorServiceImpl implements SetorService {

    @Autowired
    private SetorRepository setorRepository;

    @Override
    @Cacheable(cacheNames = "SetoresWithColaboradores", key = "#root.method.name")
    public List<Setor> agruparColaboradoresPorSetor() {
        return setorRepository.findAllWithColaboradores().stream().collect(Collectors.toList());
    }

    @Override
    public Long countColaboradoresPorSetor(Setor setor) {
        return setorRepository.countColaboradoresPorSetor(setor);
    }

    @Override
    public Long countColaboradoresPorSetorMaisVelhosQue(Setor setor, int idade) {
        return setorRepository.countColaboradoresPorSetorNasceramAntesDe(setor, LocalDate.now().minusYears(idade));
    }

    @Override
    public Long countColaboradoresSetorMaisNovosQue(Setor setor, int idade) {
        return setorRepository.countColaboradoresPorSetorNasceramApos(setor, LocalDate.now().minusYears(idade));
    }

}
