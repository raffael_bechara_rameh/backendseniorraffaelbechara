package br.com.mobicare.desafio.service.impl;

import br.com.mobicare.desafio.exception.BusinessException;
import br.com.mobicare.desafio.service.EmailService;
import br.com.mobicare.desafio.service.PessoaService;
import br.com.mobicare.desafio.utils.CpfCnpjUtils;
import br.com.mobicare.desafio.utils.EmailUtils;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    @Override
    public void validarEmail(String email) {
        if (!EmailUtils.isValid(email)) {
            throw new BusinessException("Email inválido.");
        }
    }
}
