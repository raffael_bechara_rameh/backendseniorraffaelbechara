package br.com.mobicare.desafio.service.impl;

import br.com.mobicare.desafio.exception.BusinessException;
import br.com.mobicare.desafio.service.PessoaService;
import br.com.mobicare.desafio.utils.CpfCnpjUtils;
import org.hibernate.validator.internal.constraintvalidators.hv.br.CPFValidator;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintValidatorContext;

@Service
public class PessoaServiceImpl implements PessoaService {

    @Override
    public void validarCpf(String cpf) {
        if (!CpfCnpjUtils.isValid(cpf)) {
            throw new BusinessException("CPF inválido.");
        };
    }


}
