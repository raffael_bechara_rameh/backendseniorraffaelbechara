package br.com.mobicare.desafio.service;

import br.com.mobicare.desafio.domain.Colaborador;
import br.com.mobicare.desafio.domain.Setor;

import java.util.List;

public interface ColaboradorService {

    Colaborador inserir(Colaborador colaborador, Setor setor);
    void remover(Colaborador colaborador);
    List<Colaborador> recuperarPorSetor(Setor setor);
}
