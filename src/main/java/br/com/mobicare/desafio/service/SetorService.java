package br.com.mobicare.desafio.service;

import br.com.mobicare.desafio.domain.Setor;

import java.util.List;

public interface SetorService {

    List<Setor> agruparColaboradoresPorSetor();

    Long countColaboradoresPorSetor(Setor setor);
    Long countColaboradoresPorSetorMaisVelhosQue(Setor setor, int idade);
    Long countColaboradoresSetorMaisNovosQue(Setor setor, int idade);

}
