package br.com.mobicare.desafio.service.impl;

import br.com.mobicare.desafio.domain.Colaborador;
import br.com.mobicare.desafio.domain.Setor;
import br.com.mobicare.desafio.domain.colaborador.constants.ColaboradorConstants;
import br.com.mobicare.desafio.domain.colaborador.constants.ColaboradorMessages;
import br.com.mobicare.desafio.exception.BusinessException;
import br.com.mobicare.desafio.repository.ColaboradorRepository;
import br.com.mobicare.desafio.service.ColaboradorService;
import br.com.mobicare.desafio.service.EmailService;
import br.com.mobicare.desafio.service.PessoaService;
import br.com.mobicare.desafio.service.SetorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ColaboradorServiceImpl implements ColaboradorService {

    @Autowired
    private ColaboradorRepository colaboradorRepository;

    @Autowired
    private SetorService setorService;

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private EmailService emailService;

    @Override
    public Colaborador inserir(Colaborador colaborador, Setor setor) {
        colaborador.setSetor(setor);
        validarIdade(colaborador);
        validarCpf(colaborador);
        validarEmail(colaborador);
        return colaboradorRepository.save(colaborador);
    }

    @Override
    public void remover(Colaborador colaborador) {
        colaboradorRepository.delete(colaborador);
    }

    @Override
    public List<Colaborador> recuperarPorSetor(Setor setor) {
        return colaboradorRepository.findAllBySetor(setor);
    }

    private void validarIdade(Colaborador colaborador) {
        Long qtdColaboradoresPorSetor = setorService.countColaboradoresPorSetor(colaborador.getSetor());

        validarIdadeMinima(colaborador, qtdColaboradoresPorSetor,
                setorService.countColaboradoresSetorMaisNovosQue(colaborador.getSetor(), ColaboradorConstants.IDADE_MINIMA));

        validarIdadeMaxima(colaborador, qtdColaboradoresPorSetor,
                setorService.countColaboradoresPorSetorMaisVelhosQue(colaborador.getSetor(), ColaboradorConstants.IDADE_MAXIMA));
    }

    private void validarIdadeMinima(Colaborador colaborador, Long qtdColaboradoresPorSetor, Long qtdColaboradoresPorSetorMenores18Anos) {
        if (qtdColaboradoresPorSetor > 0) {
            Long percentualMenores18Anos = 100 * qtdColaboradoresPorSetorMenores18Anos / qtdColaboradoresPorSetor;
            if (colaborador.getDataNascimento().isAfter(LocalDate.now().minusYears(ColaboradorConstants.IDADE_MINIMA)) &&
                    percentualMenores18Anos > ColaboradorConstants.PERCENTUAL_MAXIMO_PERMITIDO_COLABORADORES_MENORES_18_ANOS) {
                throw new BusinessException(ColaboradorMessages.LIMITE_DE_COLABORADORES_MENORES_QUE_18_ANOS_EXCEDIDO);
            }
        }
    }

    private void validarIdadeMaxima(Colaborador colaborador, Long qtdColaboradoresPorSetor, Long qtdColaboradoresPorSetorMaiores65Anos) {
        if (qtdColaboradoresPorSetor > 0) {
            Long percentualMaiores65Anos = 100 * qtdColaboradoresPorSetorMaiores65Anos / qtdColaboradoresPorSetor;
            if (colaborador.getDataNascimento().isBefore(LocalDate.now().minusYears(ColaboradorConstants.IDADE_MAXIMA)) &&
                    percentualMaiores65Anos > ColaboradorConstants.PERCENTUAL_MAXIMO_PERMITIDO_COLABORADORES_MAIORES_65_ANOS) {
                throw new BusinessException(ColaboradorMessages.LIMITE_DE_COLABORADORES_MAIORES_QUE_65_ANOS_EXCEDIDO);
            }
        }
    }

    private void validarCpf(Colaborador colaborador) {
        pessoaService.validarCpf(colaborador.getCpf());
    }

    private void validarEmail(Colaborador colaborador) {
        emailService.validarEmail(colaborador.getEmail());
    }
}
