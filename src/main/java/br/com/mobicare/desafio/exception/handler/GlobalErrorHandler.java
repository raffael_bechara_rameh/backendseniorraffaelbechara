package br.com.mobicare.desafio.exception.handler;

import br.com.mobicare.desafio.exception.BusinessException;
import br.com.mobicare.desafio.exception.dto.ErrorResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalErrorHandler  {

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value= HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponseDTO handleResourceNotFoundException(ResourceNotFoundException ex) {
        return ErrorResponseDTO.builder().message(ex.getMessage()).build();
    }

    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponseDTO handleBusinessException(BusinessException ex) {
        return ErrorResponseDTO.builder().message(ex.getMessage()).build();
    }


}
