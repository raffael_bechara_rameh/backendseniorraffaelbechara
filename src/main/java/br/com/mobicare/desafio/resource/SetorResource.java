package br.com.mobicare.desafio.resource;

import br.com.mobicare.desafio.domain.Setor;
import br.com.mobicare.desafio.service.SetorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("setores")
public class SetorResource {

    @Autowired
    private SetorService setorService;

    @GetMapping("colaboradores-agrupados")
    public List<Setor> agruparColaboradoresPorSetor() {
        return setorService.agruparColaboradoresPorSetor();
    }
}
