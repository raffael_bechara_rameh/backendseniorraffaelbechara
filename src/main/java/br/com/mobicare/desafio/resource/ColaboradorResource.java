package br.com.mobicare.desafio.resource;

import br.com.mobicare.desafio.domain.Colaborador;
import br.com.mobicare.desafio.domain.Setor;
import br.com.mobicare.desafio.domain.colaborador.constants.ColaboradorMessages;
import br.com.mobicare.desafio.domain.setor.constants.SetorMessages;
import br.com.mobicare.desafio.exception.handler.ResourceNotFoundException;
import br.com.mobicare.desafio.service.ColaboradorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("setores/{setorId}/colaboradores")
public class ColaboradorResource {

    @Autowired
    private ColaboradorService colaboradorService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Colaborador inserir(@PathVariable(name = "setorId") Optional<Setor> setor,
                               @RequestBody @Valid Colaborador colaborador) {
        checkSetor(setor);
        return colaboradorService.inserir(colaborador, setor.get());
    }

    @DeleteMapping("{colaboradorId}")
    public ResponseEntity<?> remover(@PathVariable(name = "setorId") Optional<Setor> setor,
                                     @PathVariable(name = "colaboradorId") Optional<Colaborador> colaborador) {
        checkSetor(setor);
        checkColaborador(colaborador);
        colaboradorService.remover(colaborador.get());
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{colaboradorId}")
    public Colaborador recuperarPorId(@PathVariable(name = "setorId") Optional<Setor> setor,
                                      @PathVariable(name = "colaboradorId") Optional<Colaborador> colaborador) {
        checkSetor(setor);
        return checkColaborador(colaborador);
    }

    private Colaborador checkColaborador(Optional<Colaborador> colaborador) {
        return colaborador.orElseThrow(() -> new ResourceNotFoundException(ColaboradorMessages.COLABORADOR_INEXISTENTE));
    }

    private void checkSetor(@PathVariable(name = "setorId") Optional<Setor> setor) {
        setor.orElseThrow(() -> new ResourceNotFoundException(SetorMessages.SETOR_NAO_ENCONTRADO));
    }
}
