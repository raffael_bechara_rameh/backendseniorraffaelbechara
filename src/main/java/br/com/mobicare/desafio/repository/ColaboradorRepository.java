package br.com.mobicare.desafio.repository;

import br.com.mobicare.desafio.domain.Colaborador;
import br.com.mobicare.desafio.domain.Setor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColaboradorRepository extends CrudRepository<Colaborador, Long> {

    List<Colaborador> findAllBySetor(Setor setor);
}
