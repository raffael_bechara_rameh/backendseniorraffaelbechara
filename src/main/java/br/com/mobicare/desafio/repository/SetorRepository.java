package br.com.mobicare.desafio.repository;

import br.com.mobicare.desafio.domain.Setor;
import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Repository
public interface SetorRepository extends CrudRepository<Setor, Long> {

    @Query(value = "select distinct setor, colaboradores from Setor setor join fetch setor.colaboradores colaboradores",
            countQuery = "select count(setor) from Setor setor join fetch setor.colaboradores colaboradores")
    List<Setor> findAllWithColaboradores();

    @Query("select count(c) from Colaborador c where c.setor = :setor")
    Long countColaboradoresPorSetor(Setor setor);

    @Query("select count(c) from Colaborador c where c.setor = :setor and c.dataNascimento <= :data")
    Long countColaboradoresPorSetorNasceramAntesDe(Setor setor, LocalDate data);

    @Query("select count(c) from Colaborador c where c.setor = :setor and c.dataNascimento >= :data")
    Long countColaboradoresPorSetorNasceramApos(Setor setor, LocalDate data);
}
