package br.com.mobicare.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BackendSeniorRaffaelBecharaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendSeniorRaffaelBecharaApplication.class, args);
	}

}
