package br.com.mobicare.desafio.domain.colaborador.constants;

public class ColaboradorConstants {

    public static final int IDADE_MINIMA = 18;
    public static final int IDADE_MAXIMA = 65;
    public static final int PERCENTUAL_MAXIMO_PERMITIDO_COLABORADORES_MENORES_18_ANOS = 20;
    public static final int PERCENTUAL_MAXIMO_PERMITIDO_COLABORADORES_MAIORES_65_ANOS = 20;
}
