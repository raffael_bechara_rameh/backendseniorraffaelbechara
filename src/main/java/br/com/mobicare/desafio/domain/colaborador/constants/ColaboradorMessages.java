package br.com.mobicare.desafio.domain.colaborador.constants;

public class ColaboradorMessages {
    public static final String LIMITE_DE_COLABORADORES_MENORES_QUE_18_ANOS_EXCEDIDO = "Limite de colaboradores menores que 18 anos excedido.";
    public static final String LIMITE_DE_COLABORADORES_MAIORES_QUE_65_ANOS_EXCEDIDO = "Limite de colaboradores maiores que 65 anos excedido.";
    public static final String COLABORADOR_INEXISTENTE = "Colaborador inexistente.";
}
