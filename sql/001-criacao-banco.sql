--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2019-11-30 22:10:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 17006)
-- Name: colaborador; Type: TABLE; Schema: public; Owner: desafio
--

CREATE TABLE public.colaborador (
    id bigint NOT NULL,
    cpf character varying(255),
    data_nascimento date NOT NULL,
    email character varying(255),
    nome character varying(255),
    telefone character varying(255),
    setor_id bigint
);


ALTER TABLE public.colaborador OWNER TO desafio;

--
-- TOC entry 202 (class 1259 OID 17004)
-- Name: colaborador_id_seq; Type: SEQUENCE; Schema: public; Owner: desafio
--

CREATE SEQUENCE public.colaborador_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.colaborador_id_seq OWNER TO desafio;

--
-- TOC entry 2832 (class 0 OID 0)
-- Dependencies: 202
-- Name: colaborador_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: desafio
--

ALTER SEQUENCE public.colaborador_id_seq OWNED BY public.colaborador.id;


--
-- TOC entry 205 (class 1259 OID 17017)
-- Name: setor; Type: TABLE; Schema: public; Owner: desafio
--

CREATE TABLE public.setor (
    id bigint NOT NULL,
    descricao character varying(255)
);


ALTER TABLE public.setor OWNER TO desafio;

--
-- TOC entry 204 (class 1259 OID 17015)
-- Name: setor_id_seq; Type: SEQUENCE; Schema: public; Owner: desafio
--

CREATE SEQUENCE public.setor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setor_id_seq OWNER TO desafio;

--
-- TOC entry 2833 (class 0 OID 0)
-- Dependencies: 204
-- Name: setor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: desafio
--

ALTER SEQUENCE public.setor_id_seq OWNED BY public.setor.id;


--
-- TOC entry 2694 (class 2604 OID 17009)
-- Name: colaborador id; Type: DEFAULT; Schema: public; Owner: desafio
--

ALTER TABLE ONLY public.colaborador ALTER COLUMN id SET DEFAULT nextval('public.colaborador_id_seq'::regclass);


--
-- TOC entry 2695 (class 2604 OID 17020)
-- Name: setor id; Type: DEFAULT; Schema: public; Owner: desafio
--

ALTER TABLE ONLY public.setor ALTER COLUMN id SET DEFAULT nextval('public.setor_id_seq'::regclass);


--
-- TOC entry 2697 (class 2606 OID 17014)
-- Name: colaborador colaborador_pkey; Type: CONSTRAINT; Schema: public; Owner: desafio
--

ALTER TABLE ONLY public.colaborador
    ADD CONSTRAINT colaborador_pkey PRIMARY KEY (id);


--
-- TOC entry 2699 (class 2606 OID 17022)
-- Name: setor setor_pkey; Type: CONSTRAINT; Schema: public; Owner: desafio
--

ALTER TABLE ONLY public.setor
    ADD CONSTRAINT setor_pkey PRIMARY KEY (id);


--
-- TOC entry 2700 (class 2606 OID 17023)
-- Name: colaborador fkia1xdsbmy6pkus4mrgy90c3he; Type: FK CONSTRAINT; Schema: public; Owner: desafio
--

ALTER TABLE ONLY public.colaborador
    ADD CONSTRAINT fkia1xdsbmy6pkus4mrgy90c3he FOREIGN KEY (setor_id) REFERENCES public.setor(id);


-- Completed on 2019-11-30 22:10:04

--
-- PostgreSQL database dump complete
--

